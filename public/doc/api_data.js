define({ "api": [
  {
    "type": "post",
    "url": "/authentication/register",
    "title": "To register a user",
    "name": "PostRegister",
    "group": "Authentication",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Mandatory user email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mandatory user password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "POST /authentication/register HTTP/1.1\nContent-Type: application/json\nHost: localhost:6789\n\n{\n    email: \"user@example.com\",\n    password: \"a53c43cdd405e427728a4fef4dc4a8c6746e1b43b5f6b4a7c84791f83e968e959b4e4b74ab91f3094077e3892fd47de1003177d755a98d84f5457bba99aceafc\"\n}",
          "type": "jspn"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "result",
            "description": "<p>Register result.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Sucess-Response:",
          "content": "HTTP/1.1 201 Created\nContent-Type: application/json; charset=utf-8\n\n{\n    \"message\": \"Register success.\",\n    \"result\":\n    {\n        \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJpYXQiOjE1MDc1NjY3NDd9.fHDI-T0bRybEU2ztX4OwNwUQ4O1wiIAy3LFnEfvu7nI\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "result",
            "description": "<p>Register result.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 409 Conflict\nContent-Type: application/json; charset=utf-8\n\n{\n    \"message\": \"Register failed.\",\n    \"reason\": \"User already exists.\"\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "HTTPie",
        "content": "http --verbose :6789/authentication/register email=user@example.com password=secret",
        "type": "HTTPie"
      }
    ],
    "version": "0.0.0",
    "filename": "services/authentication/router.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/sign_in",
    "title": "To sign in as user",
    "name": "PostSignIn",
    "group": "Authentication",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Mandatory user email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mandatory user password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example params:",
          "content": "{\n    email: \"user@example.com\",\n    password: \"a53c43cdd405e427728a4fef4dc4a8c6746e1b43b5f6b4a7c84791f83e968e959b4e4b74ab91f3094077e3892fd47de1003177d755a98d84f5457bba99aceafc\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "result",
            "description": "<p>Register result.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example success:",
          "content": "HTTP/1.1 200 OK\n\n{\n    \"message\": \"Sign in success.\",\n    \"result\":\n    {\n        \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJpYXQiOjE1MDc1NjY3NDd9.fHDI-T0bRybEU2ztX4OwNwUQ4O1wiIAy3LFnEfvu7nI\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "result",
            "description": "<p>Register result.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example failed:",
          "content": "HTTP/1.1 409 Conflict\n\n{\n    \"message\": \"Sign in failed.\",\n    \"reason\": \"Empty email.\"\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "HTTPie",
        "content": "http --verbose :6789/authentication/sign_in email=user@example.com password=secret",
        "type": "HTTPie"
      }
    ],
    "version": "0.0.0",
    "filename": "services/authentication/router.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "get",
    "url": "/persistence/:path",
    "title": "To read data",
    "name": "GetPersistence",
    "group": "Persistence",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>Mandatory data location.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "GET /persistence/assets HTTP/1.1\nHost: localhost:6789",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "result",
            "description": "<p>Persistence result.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Sucess-Response:",
          "content": "HTTP/1.1 200 OK\nContent-Type: application/json; charset=utf-8\n\n{\n    \"message\": \"Persistence success.\",\n    \"result\": [\n        {\n            \"_id\": \"59dd4fa3708de1001ef9282b\",\n            \"field00\": \"value00\",\n            \"field01\": \"value01\"\n        },\n        {\n            \"_id\": \"59dd4fde146cd4002b3cdffb\",\n            \"field10\": \"value10\",\n            \"field11\": \"value11\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "result",
            "description": "<p>Persistence result.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 409 Conflict\nContent-Type: application/json; charset=utf-8\n\n{\n    \"message\": \"Persistence failed.\",\n    \"reason\": \"Error: failed to reconnect after 30 attempts with interval 1000 ms.\"\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "HTTPie",
        "content": "http --verbose :6789/persistence/assets",
        "type": "HTTPie"
      }
    ],
    "version": "0.0.0",
    "filename": "services/persistence/router.js",
    "groupTitle": "Persistence"
  },
  {
    "type": "get",
    "url": "/persistence/:path/:id",
    "title": "To read details",
    "name": "GetPersistenceById",
    "group": "Persistence",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>Mandatory data location.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Mandatory data id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "GET /persistence/assets/59dd4fa3708de1001ef9282b HTTP/1.1\nHost: localhost:6789",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "result",
            "description": "<p>Persistence result.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Sucess-Response:",
          "content": "HTTP/1.1 200 OK\nContent-Type: application/json; charset=utf-8\n\n{\n    \"message\": \"Persistence sucess.\",\n    \"result\": {\n        \"_id\": \"59dd4fa3708de1001ef9282b\",\n        \"field00\": \"value00\",\n        \"field01\": \"value01\"\n    }\n}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "result",
            "description": "<p>Persistence result.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 409 Conflict\nContent-Type: application/json; charset=utf-8\n\n{\n    \"message\": \"Persistence failed.\",\n    \"reason\": \"Error: failed to reconnect after 30 attempts with interval 1000 ms.\"\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "HTTPie",
        "content": "http --verbose :6789/persistence/assets/59dd4fa3708de1001ef9282b",
        "type": "HTTPie"
      }
    ],
    "version": "0.0.0",
    "filename": "services/persistence/router.js",
    "groupTitle": "Persistence"
  },
  {
    "type": "post",
    "url": "/persistence/:path",
    "title": "To store data",
    "name": "PostPersistence",
    "group": "Persistence",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>Mandatory location.</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "body",
            "description": "<p>Mandatory data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "POST /persistence/assets HTTP/1.1\nAuthorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFsaWNlLjc1N2U4YzAyNzVmYWU1ZTNiNDYzZWRkMTc2YzE2NzhjQGV4YW1wbGUuY29tIiwicGFzc3dvcmQiOiJzZWNyZXQiLCJpYXQiOjE1MDc2NzA5ODF9.3ZkR0E1jJdkwuaVH8222KYl1l2fYXp0Ijof3PzvoZoc\nContent-Type: application/json\nHost: localhost:6789\n\n{\n   \"field00\": \"value00\",\n   \"field01\": \"value01\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "result",
            "description": "<p>Persistence result.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Sucess-Response:",
          "content": "HTTP/1.1 201 Created\nContent-Type: application/json; charset=utf-8\n\n{\n    \"message\": \"Persistence success.\",\n    \"result\": {\n        \"_id\": \"59dd4fa3708de1001ef9282b\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "result",
            "description": "<p>Persistence result.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 409 Conflict\nContent-Type: application/json; charset=utf-8\n\n{\n    \"message\": \"Persistence failed.\",\n    \"reason\": \"Error: Rule break.\"\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "HTTPie",
        "content": "http --verbose :6789/persistence/assets field00=value00 field01=value01 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFsaWNlLjc1N2U4YzAyNzVmYWU1ZTNiNDYzZWRkMTc2YzE2NzhjQGV4YW1wbGUuY29tIiwicGFzc3dvcmQiOiJzZWNyZXQiLCJpYXQiOjE1MDc2NzA5ODF9.3ZkR0E1jJdkwuaVH8222KYl1l2fYXp0Ijof3PzvoZoc'",
        "type": "HTTPie"
      }
    ],
    "version": "0.0.0",
    "filename": "services/persistence/router.js",
    "groupTitle": "Persistence"
  },
  {
    "type": "post",
    "url": "/search/:route",
    "title": "To find data",
    "name": "PostSearch",
    "group": "Search",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "query",
            "description": "<p>MongoDB based query.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example params:",
          "content": "{\n  field: [string],\n  another_field: [numeric],\n  ...\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "details",
            "description": "<p>Search result.</p>"
          }
        ],
        "Success 204": [
          {
            "group": "Success 204",
            "type": "json",
            "optional": false,
            "field": "NoContent",
            "description": "<p>Any result found in database.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example data on success:",
          "content": "HTTP/1.1 200 OK\n{\n    \"details\": [\n        {\n            \"_id\": \"59d804f40b58e0001049bace\",\n            \"dummy_field\": \"dummy_value_1\"\n        }\n    ],\n    \"message\": \"OK\"\n}",
          "type": "json"
        },
        {
          "title": "No Content Response:",
          "content": "HTTP/1.1 204 No Content\nmessage: No Content",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "httpie",
        "content": "http :6789/persistence/dummy_search_route dummy_field=dummy_value_0\nhttp :6789/search/dummy_search_route dummy_field=dummy_value_0\nhttp :6789/search/dummy_search_route dummy_field=dummy_value_1",
        "type": "httpie"
      },
      {
        "title": "curl",
        "content": "curl --verbose -H \"Content-Type: application/json\" -X POST -d '{\"dummy_field\":\"dummy_value_0\"}' http://localhost:6789/persistence/dummy_search_route\ncurl --verbose -H \"Content-Type: application/json\" -X POST -d '{\"dummy_field\":\"dummy_value_0\"}' http://localhost:6789/search/dummy_search_route\ncurl --verbose -H \"Content-Type: application/json\" -X POST -d '{\"dummy_field\":\"dummy_value_1\"}' http://localhost:6789/search/dummy_search_route",
        "type": "curl"
      }
    ],
    "version": "0.0.0",
    "filename": "services/search/router.js",
    "groupTitle": "Search"
  }
] });
