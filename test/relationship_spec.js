// Test the relationship service
/* eslint-env mocha */

const debug = require('debug')('yabaas:relationship-test')  // eslint-disable-line

const chakram = require('chakram')
const expect = chakram.expect

const config = require('config')
const apiUrl = 'http://' + config.get('api.host') + ':' + config.get('api.port')

/** @test {Relationship} */
describe('Relationship API Test Suite:', () => {
  let id0 = ''
  let id1 = ''

  before('Initialize foreign', () => {
    const r0 = chakram.post(apiUrl + '/persistence/foreign', {value: 'value_0'})
    expect(r0).to.have.status(201)
    expect(r0).to.have.json(json => {
      // debug(json)
      id0 = json.result._id
    })
    const r1 = chakram.post(apiUrl + '/persistence/foreign', {value: 'value_1'})
    expect(r1).to.have.status(201)
    expect(r1).to.have.json(json => {
      id1 = json.result._id
    })
    const r2 = chakram.post(apiUrl + '/persistence/foreign', {value: 'value_2'})
    expect(r2).to.have.status(201)
    const r3 = chakram.post(apiUrl + '/persistence/foreign', {value: 'value_3'})
    expect(r3).to.have.status(201)
    return chakram.wait()
  })

  it('Create a relation', () => {
    return chakram.post(apiUrl + '/relationship', {route: 'route', key: 'key', foreign: 'foreign', value: id0})
      .then((response) => {
        expect(response).to.have.status(201)
        expect(response).to.have.json(json => {
          expect(json.result).to.have.length(1)
        })
      })
  })

  it('Get all associated documents', () => {
    const response = chakram.get(apiUrl + '/relationship/route/key/foreign')
    expect(response).to.have.status(200)
    expect(response).to.have.json(json => {
      // debug(json)
      expect(json.result).to.have.length(1)
      expect(json.result[0]).to.equal(id0.toString())
      expect(json.details[0]._id).to.equal(id0.toString())
      expect(json.details[0].value).to.equal('value_0')
    })
    return chakram.wait()
  })

  it('Create a second relation', () => {
    return chakram.post(apiUrl + '/relationship', {route: 'route', key: 'key', foreign: 'foreign', value: id1})
      .then((response) => {
        expect(response).to.have.status(201)
        expect(response).to.have.json(json => {
          expect(json.result).to.have.length(1)
        })
      })
  })

  it('Get all associated documents', () => {
    const response = chakram.get(apiUrl + '/relationship/route/key/foreign')
    expect(response).to.have.status(200)
    expect(response).to.have.json(json => {
      // debug(json)
      expect(json.result).to.have.length(2)
      expect(json.result[0]).to.equal(id0.toString())
      expect(json.result[1]).to.equal(id1.toString())
    })
    return chakram.wait()
  })

  it('Can not create same relation twice', () => {
    return chakram.post(apiUrl + '/relationship', {route: 'route', key: 'key', foreign: 'foreign', value: id1})
      .then((response) => {
        expect(response).to.have.status(409)
      })
  })

  it('Delete a relation', () => {
    const response = chakram.delete(apiUrl + '/relationship/route/key/foreign/' + id1)
    expect(response).to.have.status(201)
    return chakram.wait()
  })

  it('Get all associated documents', () => {
    const response = chakram.get(apiUrl + '/relationship/route/key/foreign')
    expect(response).to.have.status(200)
    expect(response).to.have.json(json => {
      // debug(json)
      expect(json.result).to.have.length(1)
      expect(json.result[0]).to.equal(id0.toString())
    })
    return chakram.wait()
  })
})
