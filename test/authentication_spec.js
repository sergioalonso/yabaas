/**
 *  Test the token based authentication service
 */
/* eslint-env mocha */

const debug = require('debug')('yabaas:filter-test') // eslint-disable-line

const chakram = require('chakram')
const expect = chakram.expect
const wait = chakram.wait

const config = require('config')
const url = config.get('api.url')

describe('Authentication API Test Suite:', () => {
  let token = ''

  it('Register new user', () => {
    const response = chakram.post(url + '/authentication/register', {email: 'user@example.com', password: 'secret'})
    expect(response).to.have.status(201)
    expect(response).to.have.json('message', 'Register success.')
    expect(response).to.have.json(res => {
      token = res.result.token
    })
    return wait()
  })

  it('Email should be unique', () => {
    const response = chakram.post(url + '/authentication/register', {email: 'user@example.com', password: 'secret'})
    expect(response).to.have.status(409)
    expect(response).to.have.json('message', 'Register failed.')
    expect(response).to.have.json('reason', 'Error: Rule break.')
    return wait()
  })

  it('Email should not be empty', () => {
    const response = chakram.post(url + '/authentication/register', {email: '', password: 'secret'})
    expect(response).to.have.status(409)
    expect(response).to.have.json('message', 'Register failed.')
    expect(response).to.have.json('reason', 'Error: Rule break.')
    return wait()
  })

  it('A new user could be registered', () => {
    const response = chakram.post(url + '/authentication/register', {email: 'new_user@example.com', password: 'new_secret'})
    expect(response).to.have.status(201)
    expect(response).to.have.json('message', 'Register success.')
    expect(response).to.have.json(res => {
      expect(token).not.to.be.equal(res.result.token)
    })
    return wait()
  })

  it('Valid email could sign in', () => {
    const response = chakram.post(url + '/authentication/sign_in', {email: 'user@example.com', password: 'secret'})
    expect(response).to.have.status(200)
    expect(response).to.have.json('message', 'Sign in success.')
    return wait()
  })

  it('Invalid email could not sign in', () => {
    const response = chakram.post(url + '/authentication/sign_in', {email: 'bad_user@example.com', password: 'bad_secret'})
    expect(response).to.have.status(401)
    expect(response).to.have.json('message', 'Sign in failed.')
    expect(response).to.have.json('reason', 'Error: User not found.')
    return wait()
  })

  it('Email should not be empty for sign in', () => {
    const response = chakram.post(url + '/authentication/sign_in', {email: '', password: ''})
    expect(response).to.have.status(401)
    expect(response).to.have.json('message', 'Sign in failed.')
    expect(response).to.have.json('reason', 'Error: User not found.')
    return wait()
  })

  it('Email is required for sign in', () => {
    const response = chakram.post(url + '/authentication/sign_in', {password: 'secret'})
    expect(response).to.have.status(401)
    expect(response).to.have.json('message', 'Sign in failed.')
    expect(response).to.have.json('reason', 'Error: User not found.')
    return wait()
  })

  it('Should not get an error altough JWT token is not provided', () => {
    const response = chakram.get(url + '/authentication/private')
    expect(response).to.have.status(200)
    expect(response).to.have.json('message', 'You can see this without a token.')
    return wait()
  })

  it('Should get an error if JWT token is not provided', () => {
    const response = chakram.post(url + '/authentication/private')
    expect(response).to.have.status(401)
    expect(response).to.have.json('Unauthorized')
    return wait()
  })

  it('Should get an error if JWT token is provided but it is not valid', () => {
    const response = chakram.post(url + '/authentication/private', {}, {headers: {'Authorization': 'Bearer bad_token'}})
    expect(response).to.have.status(401)
    expect(response).to.have.json('Unauthorized')
    return wait()
  })

  it('Should get private content if JWT token is provided and it is valid', () => {
    const response = chakram.post(url + '/authentication/private', {}, {headers: {'Authorization': 'Bearer ' + token}})
    expect(response).to.have.status(200)
    expect(response).to.have.json('message', 'You can see this with a token.')
    return wait()
  })

  it('Should get private content if is the owner', () => {
    const response = chakram.get(url + '/authentication/owner/user@example.com', {headers: {'Authorization': 'Bearer ' + token}})
    expect(response).to.have.status(200)
    expect(response).to.have.json('message', 'You can see this if you are the owner.')
    return wait()
  })

  it('Should not get private content if is not the owner', () => {
    const response = chakram.get(url + '/authentication/owner/bad_user@example.com', {headers: {'Authorization': 'Bearer ' + token}})
    expect(response).to.have.status(401)
    expect(response).to.have.json('Unauthorized')
    return wait()
  })
})
