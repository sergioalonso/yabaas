/**
 * Test the persistence service
 */
/* eslint-env mocha */

const debug = require('debug')('yabaas:persistence-test')  // eslint-disable-line

const chakram = require('chakram')
const expect = chakram.expect
const wait = chakram.wait

const fs = require('fs')
const path = require('path')

const config = require('config')
const url = config.get('api.url')

describe('Persistence API Test Suite:', () => {
  const resource = {
    id: '',
    formData: { field: 'value', attachments: fs.createReadStream(path.resolve(__dirname, '../mocks/148x210.png')) }
  }

  it('POST Method form-data', () => {
    const params = {}
    params['headers'] = {}
    params['headers']['Content-Type'] = 'multipart/form-data'
    params['formData'] = resource.formData

    const response = chakram.post(url + '/persistence/route', undefined, params)
    expect(response).to.have.status(201)
    return wait()
  })

  it('GET Method', () => {
    const response = chakram.get(url + '/persistence/route')
    expect(response).to.have.status(200)
    expect(response).to.have.json(json => {
      // debug(json)
      expect(json.result).to.have.length(1)
      expect(json.result[0].field).to.equal(resource.formData.field)
      resource.id = json.result[0]._id
    })
    return wait()
  })

  it('GET Method sort', () => {
    const response = chakram.get(url + '/persistence/route?sort=-dummy_date')
    expect(response).to.have.status(200)
    expect(response).to.have.json(json => {
      // debug(json)
      expect(json.result).to.have.length(1)
      expect(json.result[0].field).to.equal(resource.formData.field)
      resource.id = json.result[0]._id
    })
    return wait()
  })

  it('GET Method by id', () => {
    const response = chakram.get(url + '/persistence/route/' + resource.id)
    expect(response).to.have.status(200)
    expect(response).to.have.json(json => {
      // debug(json)
      expect(json.result.field).to.equal(resource.formData.field)
    })
    return wait()
  })

  it('POST Method', () => {
    const response = chakram.post(url + '/persistence/route', { field: 'value' })
    expect(response).to.have.status(201)
    return wait()
  })

  it('POST Method with slug fields', () => {
    let id = ''

    const postResponse = chakram.post(url + '/persistence/slug_dummy_route', { field: 'field value', dummy_field: 'dummy field value' })
    expect(postResponse).to.have.status(201)
    expect(postResponse).to.have.json(json => {
      // debug(json)
      id = json.result._id
    })

    const getResponse = chakram.get(url + '/persistence/slug_dummy_route/' + id)
    expect(getResponse).to.have.status(200)
    expect(getResponse).to.have.json(json => {
      // debug(json)
      expect(json.result[0].field).to.equal('field value')
      expect(json.result[0].dummy_field).to.equal('dummy field value')
      expect(json.result[0].dummy_field_slug).to.equal('dummy-field-value')
    })

    return wait()
  })
})
