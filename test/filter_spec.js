// Test the validation service
/* eslint-env mocha */

const debug = require('debug')('yabaas:filter-test') // eslint-disable-line

const chakram = require('chakram')
const expect = chakram.expect

const config = require('config')
const apiUrl = 'http://' + config.get('api.host') + ':' + config.get('api.port')

describe('Filter API Test Suite:', function () {
  let today
  let yesterday
  let tomorrow

  before('Initialize dates', function () {
    today = new Date()
    yesterday = new Date()
    yesterday.setDate(today.getDate() - 1)
    tomorrow = new Date()
    tomorrow.setDate(today.getDate() + 1)
  })

  it('Filter: dummy data: post a field value to true', function () {
    const response = chakram.post(apiUrl + '/persistence/another_dummy_route', {dummy_field: 'true', dummy_date: yesterday})
    expect(response).to.have.status(201)
    return chakram.wait()
  })

  it('Filter: dummy data: post another field value to false', function () {
    const response = chakram.post(apiUrl + '/persistence/another_dummy_route', {dummy_field: 'false', dummy_date: yesterday})
    expect(response).to.have.status(201)
    return chakram.wait()
  })

  it('Filter: equal: get only dummy fields equal to true', function () {
    const response = chakram.get(apiUrl + '/persistence/another_dummy_route')
    expect(response).to.have.status(200)
    expect(response).to.have.json(json => {
      expect(json.result).to.have.length(1)
      expect(json.result[0].dummy_field).to.equal('true')
    })
    return chakram.wait()
  })

  it('Filter: dummy data: post another field with tomorrow value', function () {
    const response = chakram.post(apiUrl + '/persistence/another_dummy_route', {dummy_field: 'true', dummy_date: tomorrow})
    expect(response).to.have.status(201)
    return chakram.wait()
  })

  it('Filter: less than: get only dummy dates before today', function () {
    const response = chakram.get(apiUrl + '/persistence/another_dummy_route')
    expect(response).to.have.status(200)
    expect(response).to.have.json(json => {
      expect(json.result).to.have.length(1)
      expect(new Date(json.result[0].dummy_date).getDate()).to.be.below(today)
    })
    return chakram.wait()
  })
})
