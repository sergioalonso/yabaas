/** Test the search service */
/* eslint-env mocha */

const debug = require('debug')('yabaas:search-test')  // eslint-disable-line

const chakram = require('chakram')
const expect = chakram.expect

const config = require('config')
const api = 'http://' + config.get('api.host') + ':' + config.get('api.port')

/** @test {Search} */
describe('Search API Test Suite:', () => {
  const response = []
  const id = []

  before('Initial data', () => {
    response[0] = chakram.post(api + '/persistence/dummy_search_route', {dummy_field: 'dummy_value_0'})
    expect(response[0]).to.have.status(201)
    expect(response[0]).to.have.json(json => {
      id[0] = json.result._id
    })

    response[1] = chakram.post(api + '/persistence/dummy_search_route', {dummy_field: 'dummy_value_1'})
    expect(response[1]).to.have.status(201)
    expect(response[1]).to.have.json(json => {
      id[1] = json.result._id
    })

    return chakram.wait()
  })

  it('Result found', () => {
    return chakram.post(api + '/search/dummy_search_route', {dummy_field: 'dummy_value_0'})
      .then((response) => {
        expect(response).to.have.status(200)
        expect(response).to.have.json('message', 'OK')
        expect(response).to.have.json(json => {
          // debug(json)
          expect(json.details).to.have.length(1)
          expect(json.details[0]._id).to.equal(id[0].toString())
          expect(json.details[0].dummy_field).to.equal('dummy_value_0')
        })
      })
  })

  it('Result not found', () => {
    return chakram.post(api + '/search/dummy_search_route', {dummy_field: 'dummy_value_9'})
      .then((response) => {
        expect(response).to.have.status(204)
        expect(response).to.have.header('message', 'No Content')
      })
  })
})
