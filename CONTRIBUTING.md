# Contributing Notes

## Virtual Domains

**Virtual domains** has been defined in `docker-compose.yml` file and configured in `/etc/hosts` file.

``` bash
echo "127.0.0.1   api.yabaas.local maildev.yabaas.local mongo.yabaas.local rabbit.yabaas.local" | sudo tee --append "/etc/hosts" > /dev/null
```

## Automation Tool

**GNU Make** has been selected to control the development process.

``` bash
$ make help
```
