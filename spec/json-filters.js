module.exports = {
  api: {
    filters: {
      another_dummy_route: {
        dummy_field: 'true',
        dummy_date: {$lte: new Date().toISOString()}
      }
    }
  }
}
