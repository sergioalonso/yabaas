module.exports = {
  api: {
    validations: {
      dummy_route: {
        dummy_field: [ 'unique', 'required' ]
      },
      register: {
        email: [ 'unique', 'required' ],
        password: [ 'required' ]
      }
    }
  }
}
