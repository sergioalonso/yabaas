// List of allowed public routes

module.exports = {
  api: {
    authentication: {
      equal_to: [
        { path: '/' }
      ],
      start_by: [
        { path: '/authentication/register' },
        { path: '/authentication/sign_in' },
        { path: '/authentication/private', method: 'GET' },
        { path: '/persistence' },
        { path: '/search' },
        { path: '/relationship' },
        { path: '/email' },
        { path: '/message' },
        { path: '/upload' }
      ],
      is_owner: [
        { path: '/authentication/owner/:user' }
      ]
    }
  }
}
