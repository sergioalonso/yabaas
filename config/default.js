const defer = require('config/defer').deferConfig
const path = require('path')

module.exports = {
  yabaas: {
    persistence: 'in_memory',
//    persistence: 'mongo',
    upload: path.join(process.cwd(), '/public')
  },
  mongo: {
    host: '127.0.0.1',
    port: '27017',
    url: defer(cfg => 'mongodb://' + cfg.mongo.host + ':' + cfg.mongo.port)
  },
  api: {
    host: '127.0.0.1',
    port: '3000',
    url: defer(cfg => 'http://' + cfg.api.host + ':' + cfg.api.port)
  },
  rabbit: {
    host: '127.0.0.1',
    port: '5672',
    user: 'admin',
    pass: 'pass'
  },
  smtp: {
    host: '127.0.0.1',
    port: '25',
    secure: false,
    from: 'no-reply@yabass.io'
  }
}
