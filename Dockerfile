FROM node:8.5.0
MAINTAINER Sergio Alonso <sergio@sergioalonso.es>
LABEL Description="Dockerfile for yabaas"

#USER node:node

RUN mkdir -p /home/node/app
WORKDIR /home/node/app

ENV PATH /home/node/app/node_modules/.bin:$PATH

COPY . /home/node/app

EXPOSE 3000
CMD ["npm", "start"]
