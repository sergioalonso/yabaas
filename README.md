# YaBaaS - Yet Another Backend as a Service

[![pipeline status](https://gitlab.com/sergioalonso/yabaas/badges/master/pipeline.svg)](https://gitlab.com/sergioalonso/yabaas/commits/master)
[![coverage report](https://gitlab.com/sergioalonso/yabaas/badges/master/coverage.svg)](https://gitlab.com/sergioalonso/yabaas/commits/master)

## Get source code

``` bash
$ git clone https://gitlab.com/sergioalonso/yabaas.git
```

## Check if everything is correct

``` bash
$ make test
```
