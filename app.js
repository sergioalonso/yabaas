const debug = require('debug')('yabaas:app')  //eslint-disable-line

const config = require('config')

const express = require('express')
const path = require('path')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const multer = require('multer')

const home = require('./services/home/router')
const persistence = require('./services/persistence/router')
const validation = require('./services/validation/router')
const authentication = require('./services/authentication/router')
const requireLogin = require('./services/authentication/passport').requireLogin
const search = require('./services/search/router')
const relationship = require('./services/relationship/router')
// const message = require('./services/message-broker/router')
// const email = require('./services/email/router')

const app = express()

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static(config.get('yabaas.upload')))

// https://enable-cors.org/server_expressjs.html
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
  res.header('Access-Control-Allow-Credentials', 'true')
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE')

  if (req.method === 'OPTIONS') {
    res.send(200)
  } else {
    next()
  }
})

app.use(multer({ dest: config.get('yabaas.upload') + '/attachments' }).any())

app.use(requireLogin)
app.use('/', home)
app.use('/persistence', persistence)
app.use('/validation', validation)
app.use('/authentication', authentication)
app.use('/search', search)
app.use('/relationship', relationship)
// app.use('/message', message)
// app.use('/email', email)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.json({message: 'error: ' + err.message})
})

module.exports = app
