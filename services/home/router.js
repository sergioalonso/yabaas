const express = require('express')
const router = express.Router()
const path = require('path')

const jsonHome = require(path.join(__dirname, 'json-home.js'))

router.get('/', function (req, res, next) {
  res.status(200).send(jsonHome)
})

router.post('/', function (req, res, next) {
  res.status(200).send({result: req.body})
})

module.exports = router
