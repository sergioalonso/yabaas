module.exports = {
  api: {
    title: process.env.API_TITLE || process.env.npm_package_title,
    version: process.env.API_VERSION || process.env.npm_package_version
  }
}
