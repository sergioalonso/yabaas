/**
 * Test the slug module
 */

/* eslint-env node, mocha */

const debug = require('debug')('yabaas:slug:test')  // eslint-disable-line

const should = require('should')  // eslint-disable-line
const expect = require('chai').expect  // eslint-disable-line

const slug = require('./index')

describe('Slug', () => {
  it('encode existing fields', () => {
    slug.encode('slug_dummy_route', {dummy_field: 'dummy value'}).should.have.property('dummy_field_slug').with.equal('dummy-value')
  })

  it('do not encode non existing fields', () => {
    expect(slug.encode('slug_dummy_route', { bad_dummy_field: 'dummy value' })).to.include({'bad_dummy_field': 'dummy value'})
  })

  it('encode more than one existing fields', () => {
    expect(slug.encode('slug_dummy_route', {dummy_field: 'dummy value', another_dummy_field: 'another dummy value'})).to.include({'dummy_field_slug': 'dummy-value', 'another_dummy_field_slug': 'another-dummy-value'})
  })
})
