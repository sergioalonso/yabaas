const slug = require('./slug')

module.exports = {
  encode: slug.encode
}
