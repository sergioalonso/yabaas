/**
 * slug module
 */

const debug = require('debug')('yabaas:slug')  // eslint-disable-mode
const path = require('path')

const config = require(path.join(process.cwd(), '/spec/json-slug')).api.slug

const slug = require('slug')

exports.encode = (path, data) => {
  debug('encode(): path: "%s", data: %o', path, data)

  const rules = config[path]
  if (typeof rules === 'undefined') {
    return data
  }
  debug('encode(): rules: %o', config[path])

  const result = data

  Object.keys(config[path]).forEach(element => {
    debug('encode(): rule key: %o', element)

    const slugKey = config[path][element]
    debug('encode(): slug key: %o', slugKey)

    const dataValue = data[element]
    debug('encode(): data value: %o', dataValue)

    if (typeof dataValue === 'undefined') {
      return
    }

    const slugValue = slug(dataValue)
    debug('encode(): slug value: %o', slugValue)

    result[slugKey] = slugValue
  })

  debug('encode(): return: %o', result)
  return result
}
