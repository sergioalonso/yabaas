/**
 * Test the persistence module
 */

/* eslint-env mocha */

const debug = require('debug')('yabaas:persistence-test:in-memory')  // eslint-disable-line

const should = require('should')  // eslint-disable-line

const persistence = require('./module')
const InMemoryStrategy = require('./strategy-in-memory')

describe('In Memory Strategy', () => {
  before(() => {
    persistence.setStrategy(InMemoryStrategy)
  })

  describe('Methods', () => {
    it('write', () => {
      persistence.create('dummy_route', {field: 'value', dummy_field: 'dummy value'}, [{fieldname: 'attachment', filename: 'image.png'}])
    })
    it('read', () => {
      persistence.read('dummy_route').then(result => result.should.have.length(1))
    })
    it('readAll', () => {
      persistence.readAll('dummy_route', {field: 'value', dummy_field: 'dummy value', dummy_field_slug: 'dummy-value', attachment: 'image.png'}).then(result => result.should.have.length(1))
    })
  })
})
