/**
 * Persistence API End Point
 *
 * POST /persistence/:path
 * GET /persistence/:path
 * GET /persistence/:path/:id
 *
 */

const debug = require('debug')('yabaas:persistence')  // eslint-disable-line

const express = require('express')
const router = express.Router()

const aqp = require('api-query-params')

const persistence = require('./module')

/**
 * @api {post} /persistence/:path To store data
 * @apiName PostPersistence
 * @apiGroup Persistence
 *
 * @apiParam {String} path Mandatory location.
 * @apiParam {json} body Mandatory data.
 * @apiParamExample {json} Request-Example:
 *     POST /persistence/assets HTTP/1.1
 *     Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFsaWNlLjc1N2U4YzAyNzVmYWU1ZTNiNDYzZWRkMTc2YzE2NzhjQGV4YW1wbGUuY29tIiwicGFzc3dvcmQiOiJzZWNyZXQiLCJpYXQiOjE1MDc2NzA5ODF9.3ZkR0E1jJdkwuaVH8222KYl1l2fYXp0Ijof3PzvoZoc
 *     Content-Type: application/json
 *     Host: localhost:6789
 *
 *     {
 *        "field00": "value00",
 *        "field01": "value01"
 *     }
 *
 * @apiSuccess {json} result Persistence result.
 * @apiSuccessExample {json} Sucess-Response:
 *     HTTP/1.1 201 Created
 *     Content-Type: application/json; charset=utf-8
 *
 *     {
 *         "message": "Persistence success.",
 *         "result": {
 *             "_id": "59dd4fa3708de1001ef9282b"
 *         }
 *     }
 *
 * @apiError {json} result Persistence result.
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 409 Conflict
 *     Content-Type: application/json; charset=utf-8
 *
 *     {
 *         "message": "Persistence failed.",
 *         "reason": "Error: Rule break."
 *     }
 *
 * @apiExample {HTTPie} HTTPie
 *     http --verbose :6789/persistence/assets field00=value00 field01=value01 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFsaWNlLjc1N2U4YzAyNzVmYWU1ZTNiNDYzZWRkMTc2YzE2NzhjQGV4YW1wbGUuY29tIiwicGFzc3dvcmQiOiJzZWNyZXQiLCJpYXQiOjE1MDc2NzA5ODF9.3ZkR0E1jJdkwuaVH8222KYl1l2fYXp0Ijof3PzvoZoc'
 *
 */

router.post('/:path', (req, res, next) => {
  const path = req.params.path
  const body = req.body
  const files = req.files

  debug('POST /persistence/%s body:%o files:%o', path, body, files)

  persistence.create(path, body, files)
    .then(result => { res.status(201).json({ message: 'Persistence success.', result: { _id: result } }) })
    .catch(reason => { res.status(409).json({ message: 'Persistence failed.', reason: reason.toString() }) })
})

/**
 * @api {get} /persistence/:path To read data
 * @apiName GetPersistence
 * @apiGroup Persistence
 *
 * @apiParam {String} path Mandatory data location.
 * @apiParamExample {json} Request-Example:
 *     GET /persistence/assets HTTP/1.1
 *     Host: localhost:6789
 *
 * @apiSuccess {json} result Persistence result.
 * @apiSuccessExample {json} Sucess-Response:
 *     HTTP/1.1 200 OK
 *     Content-Type: application/json; charset=utf-8
 *
 *     {
 *         "message": "Persistence success.",
 *         "result": [
 *             {
 *                 "_id": "59dd4fa3708de1001ef9282b",
 *                 "field00": "value00",
 *                 "field01": "value01"
 *             },
 *             {
 *                 "_id": "59dd4fde146cd4002b3cdffb",
 *                 "field10": "value10",
 *                 "field11": "value11"
 *             }
 *         ]
 *     }
 *
 * @apiError {json} result Persistence result.
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 409 Conflict
 *     Content-Type: application/json; charset=utf-8
 *
 *     {
 *         "message": "Persistence failed.",
 *         "reason": "Error: failed to reconnect after 30 attempts with interval 1000 ms."
 *     }
 *
 * @apiExample {HTTPie} HTTPie
 *     http --verbose :6789/persistence/assets
 *
 */

router.get('/:path', (req, res, next) => {
  const path = req.params.path
  const query = aqp(req.query)

  debug('GET /persistence/%s %o', path, query)

  persistence.read(path, query)
    .then(result => { res.status(200).json({ message: 'Persistence success.', result: result }) })
    .catch(reason => { res.status(409).json({ message: 'Persistence failed.', reason: reason.toString() }) })
})

/**
 * @api {get} /persistence/:path/:id To read details
 * @apiName GetPersistenceById
 * @apiGroup Persistence
 *
 * @apiParam {String} path Mandatory data location.
 * @apiParam {String} id Mandatory data id.
 * @apiParamExample {json} Request-Example:
 *     GET /persistence/assets/59dd4fa3708de1001ef9282b HTTP/1.1
 *     Host: localhost:6789
 *
 * @apiSuccess {json} result Persistence result.
 * @apiSuccessExample {json} Sucess-Response:
 *     HTTP/1.1 200 OK
 *     Content-Type: application/json; charset=utf-8
 *
 *     {
 *         "message": "Persistence sucess.",
 *         "result": {
 *             "_id": "59dd4fa3708de1001ef9282b",
 *             "field00": "value00",
 *             "field01": "value01"
 *         }
 *     }
 *     }
 *
 * @apiError {json} result Persistence result.
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 409 Conflict
 *     Content-Type: application/json; charset=utf-8
 *
 *     {
 *         "message": "Persistence failed.",
 *         "reason": "Error: failed to reconnect after 30 attempts with interval 1000 ms."
 *     }
 *
 * @apiExample {HTTPie} HTTPie
 *     http --verbose :6789/persistence/assets/59dd4fa3708de1001ef9282b
 *
 */

router.get('/:path/:id', function (req, res, next) {
  const path = req.params.path
  const id = req.params.id

  debug('GET /persistence/%s/%s', path, id)

  persistence.readById(path, id)
    .then(result => { res.status(200).json({ message: 'Persistence sucess.', result: result }) })
    .catch(reason => { res.status(409).json({ message: 'Persistence failed.', reason: reason.toString() }) })
})

module.exports = router
