/**
 * Persistence Module :: MongoDB Strategy
 */

const debug = require('debug')('yabaas:persistence:mongodb')  // eslint-disable-line
const config = require('config')  // eslint-disable-line
const url = config.get('mongo.url')

const mongo = require('mongodb').MongoClient
const ObjectId = require('mongodb').ObjectID

const isValid = require('../validation/module').isValid
const storageFilter = require('../filter/module').filter

const state = {
  db: null
}

exports.connect = () => {
  return new Promise((resolve, reject) => {
    if (state.db) return resolve(state.db)

    mongo.connect(url)
      .then(db => {
        state.db = db
        resolve(db)
      })
      .catch(err => { reject(err) })
  })
}

exports.disconnect = () => {
  return new Promise((resolve, reject) => {
    if (!state.db) return resolve()

    state.db.close()
     .then(() => {
       state.db = null
       resolve()
     })
     .catch((error) => { reject(error) })
  })
}

exports.create = (collection, document, attachment) => {
  debug('create() collection:{%s} document:%o attachment:%o', collection, document, attachment)

  if (typeof attachment !== 'undefined') {
    document[attachment[0].fieldname] = attachment[0].filename
  }

  return new Promise((resolve, reject) => {
    if (!isValid(state.db, collection, document)) {
      debug('create() reject Error: Rule break.')
      return reject(new Error('Rule break.'))
    }

    state.db.collection(collection).insertOne(document)
      .then(result => {
        debug('create() collection=[%s] result.insertedId=[%o]', collection, result.insertedId)
        resolve(result.insertedId)
      })
      .catch(reason => {
        debug('create() collection=[' + collection + '] ' + reason)
        reject(new Error('Rule break.'))
      })
  })
}

exports.read = (collection, query) => {
  debug('read() collection:{%s} query:%o', collection, query)

  const filter = storageFilter(collection)
  debug('read() collection:{%s} filter:%o', collection, filter)

  return new Promise((resolve, reject) => {
    state.db.collection(collection).find(filter).sort(query.sort).toArray()
      .then(result => { debug('read() sort %o', result); resolve(result) })
      .catch(reason => { debug('read() sort %o', reason); reject(reason) })
  })
}

exports.readAll = (collection, query) => {
  debug('readAll() collection:{%s} query:{%o}', collection, query)

  return new Promise((resolve, reject) => {
    state.db.collection(collection).find(query).toArray()
      .then(result => resolve(result))
      .catch(reason => reject(reason))
  })
}

exports.readById = (collection, id) => {
  debug('readById() collection:{%s} id:{%s}', collection, id)

  const query = {_id: new ObjectId(id)}

  return new Promise((resolve, reject) => {
    state.db.collection(collection).findOne(query)
      .then(result => resolve(result))
      .catch(reason => reject(reason))
  })
}

exports.readOne = (collection, query) => {
  debug('readOne() collection:{%s} query:{%o}', collection, query)

  return new Promise((resolve, reject) => {
    state.db.collection(collection).findOne(query)
      .then(result => resolve(result))
      .catch(reason => reject(reason))
  })
}

exports.delete = (collection, query) => {
  debug('delete() collection:{%s} query:{%o}', collection, query)

  return new Promise((resolve, reject) => {
    state.db.collection(collection).deleteOne(query)
      .then(result => resolve(result))
      .catch(reason => reject(reason))
  })
}

exports.unique = function (collection, fields) {
  debug('unique() collection:{%s} fields:{%o}', collection, fields)

  const keys = Object.keys(fields)

  state.db.collection(collection).createIndex(keys, { unique: true })

  return true
}
