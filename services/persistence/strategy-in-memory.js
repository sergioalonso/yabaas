/**
 * Persistence Module :: In Memory Strategy
 */

const debug = require('debug')('yabaas:persistence:in-memory')  // eslint-disable-line
const config = require('config')  // eslint-disable-line

const _ = require('lodash')
const crypto = require('crypto')
const compiler = require('mongo-query-compiler')

const isValid = require('../validation/module').isValid
const storageFilter = require('../filter/module').filter

const storage = []

exports.connect = () => Promise.resolve()
exports.disconnect = () => Promise.resolve()

exports.create = (path, data, files) => {
  debug('create() path: \'%s\', data: %o, files: %o', path, data, files)

  if (typeof files !== 'undefined') {
    data[files[0].fieldname] = files[0].filename
  }

  if (typeof storage[path] === 'undefined') {
    storage[path] = []
  }

  if (!isValid(storage[path], path, data)) {
    debug('create() error: Rule break.')
    return Promise.reject(new Error('Rule break.'))
  }

  data._id = crypto.randomBytes(16).toString('hex')
  storage[path].push(data)

  debug('create() path: \'%s\', _id: \'%s\'', path, data._id)
  return Promise.resolve(data._id)
}

exports.read = (path, query) => {
  debug('read() path: \'%s\', query: %o', path, query)

  const filter = storageFilter(path)
  debug('read() path: \'%s\', filter: %o', path, filter)

  if (typeof filter === 'undefined') {
    return Promise.resolve(storage[path])
  }

  if (typeof query.sort === 'undefined') {
    return Promise.resolve(_.filter(storage[path], compiler(filter)))
  }

  return Promise.resolve(_.filter(storage[path], compiler(filter)).sortBy(query.sort))
}

exports.readAll = (path, query) => {
  debug('readAll() path: \'%s\', query: %o', path, query)

  return Promise.resolve(_.filter(storage[path], query))
}

exports.readById = (path, id) => {
  debug('readById() path: \'%s\', id: \'%s\'', path, id)

  return Promise.resolve(_.filter(storage[path], { _id: id })[0])
}

exports.readOne = (path, query) => {
  debug('readOne() path: \'%s\', query: %o', path, query)

  const index = _.findIndex(storage[path], query)

  if (index === -1) {
    return Promise.resolve(null)
  }

  return Promise.resolve(storage[path][index])
}

exports.delete = (path, query) => {
  debug('delete() path: \'%s\', query: %o', path, query)

  return Promise.resolve(_.remove(storage[path], query))
}

exports.unique = (path, query) => {
  debug('unique() path: \'%s\', query: %o', path, query)

  const index = _.findIndex(storage[path], query)

  if (index === -1) {
    return true
  }

  return false
}
