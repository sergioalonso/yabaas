/**
 * Persistence Module
 */

const debug = require('debug')('yabaas:persistence:strategy')  // eslint-disable-line

const config = require('config')

const slug = require('../slug')

const inMemoryStrategy = require('./strategy-in-memory')
const mongoStrategy = require('./strategy-mongodb')

const strategy = {
  avaliable: {
    in_memory: inMemoryStrategy,
    mongo: mongoStrategy
  },
  current: null
}

exports.setStrategy = s => {
  if (strategy.current) strategy.current.disconnect()

  strategy.current = s
  strategy.current.connect()
    .catch(() => {
      debug('Unable to connect')
      process.exit(1)
    })
}

exports.getStrategy = () => strategy.current

exports.create = (path, data, files) => {
  data = slug.encode(path, data)

  return this.getStrategy().create(path, data, files)
}

exports.read = (path, query) => this.getStrategy().read(path, query)

exports.readAll = (path, query) => this.getStrategy().readAll(path, query)

exports.readById = (path, id) => this.getStrategy().readById(path, id)

exports.readOne = (path, query) => this.getStrategy().readOne(path, query)

exports.delete = (path, query) => this.getStrategy().delete(path, query)

exports.unique = (path, query) => this.getStrategy().unique(path, query)

this.setStrategy(strategy.avaliable[config.get('yabaas.persistence')])
