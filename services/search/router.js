/**
 * Search API End Point
 */

const debug = require('debug')('yabaas:search')  // eslint-disable-line

const express = require('express')
const router = express.Router()

const service = require('./module')

/**
 * @api {post} /search/:route To find data
 * @apiName PostSearch
 * @apiGroup Search
 *
 * @apiParam {json} query MongoDB based query.
 * @apiParamExample Example params:
 *     {
 *       field: [string],
 *       another_field: [numeric],
 *       ...
 *     }
 *
 * @apiSuccess {json} details Search result.
 * @apiSuccessExample {json} Example data on success:
 *     HTTP/1.1 200 OK
 *     {
 *         "details": [
 *             {
 *                 "_id": "59d804f40b58e0001049bace",
 *                 "dummy_field": "dummy_value_1"
 *             }
 *         ],
 *         "message": "OK"
 *     }
 *
 * @apiSuccess (Success 204) {json} NoContent Any result found in database.
 * @apiSuccessExample {json} No Content Response:
 *     HTTP/1.1 204 No Content
 *     message: No Content
 *
 * @apiExample {httpie} httpie
 *     http :6789/persistence/dummy_search_route dummy_field=dummy_value_0
 *     http :6789/search/dummy_search_route dummy_field=dummy_value_0
 *     http :6789/search/dummy_search_route dummy_field=dummy_value_1
 *
 * @apiExample {curl} curl
 *     curl --verbose -H "Content-Type: application/json" -X POST -d '{"dummy_field":"dummy_value_0"}' http://localhost:6789/persistence/dummy_search_route
 *     curl --verbose -H "Content-Type: application/json" -X POST -d '{"dummy_field":"dummy_value_0"}' http://localhost:6789/search/dummy_search_route
 *     curl --verbose -H "Content-Type: application/json" -X POST -d '{"dummy_field":"dummy_value_1"}' http://localhost:6789/search/dummy_search_route
 */

router.post('/:route', (req, res, next) => {
  const route = req.params.route
  const query = req.body
  debug('POST /search/%s %o', route, query)

  service.search(route, query)
    .then(result => {
      if (result === null) {
        debug('204 NO CONTENT')
        return res.status(204).header('message', 'No Content').send()
      }
      debug('200 OK %o', result)
      res.status(200).json({ message: 'OK', details: [result] })
    })
    .catch(reason => {
      debug('409 Conflict %o', reason)
      res.status(409).json({ message: 'Conflict', details: reason })
    })
})

module.exports = router
