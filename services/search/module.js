/**
 * Search API Module
 */

const debug = require('debug')('yabaas:search')  // eslint-disable-line

const persistence = require('../persistence/module')

/**
 * To read something from database
   * @param {string} collection - where to look for.
   * @param {string} query - what to look for.
   * @return {promise} result of the search.
 */
exports.search = function (collection, query) {
  debug('search() collection=[%o] query=[%o]', collection, query)

  return new Promise((resolve, reject) => {
    persistence.readOne(collection, query)
      .then(result => {
        debug('search() result=[%o]', result)
        resolve(result)
      })
      .catch(reason => {
        debug('search() reason=[%o]', reason)
        reject(reason)
      })
  })
}
