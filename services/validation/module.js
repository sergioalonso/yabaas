// Validation Module
//

const debug = require('debug')('yabaas:validation')
const path = require('path')

const jsonValidations = require(path.join(process.cwd(), '/spec/json-validations'))

const persistence = require('../persistence/module')

const unique = function (db, route, field, data) {  // eslint-disable-line
  debug('unique(): route=[' + route + '] field=[' + field + '] ' + persistence.unique(route, data))
  return persistence.unique(route, data)
}

const includesAll = function (needles, haystack) {
  debug('includesAll(): every element in [' + needles + '] is included in [' + haystack + ']')
  return needles.every(needle => haystack.includes(needle))
}

const required = function (db, route, fields) {  // eslint-disable-line
  debug('required(): route=[' + route + '] fields=[' + fields + ']')
  const allRuleKeys = Object.keys(rules[route])
  debug('required(): rules[' + route + ']=[' + allRuleKeys + ']')

  // Get keys marked as required

  let keys = []

  allRuleKeys.forEach(element => {
    if (rules[route][element].indexOf('required') !== -1) {
      keys.push(element)
    }
  })

  debug('required(): all required elements: keys=[' + keys + ']')

  if (includesAll(keys, fields)) {
    debug('required(): true')
    return true
  }

  debug('required(): false')
  return false
}

// Rule Tree
// route | field | constraints
let rules = jsonValidations.api.validations

exports.create = function (route, data) {
  debug('validation:create() route=[' + route + '] rules=[' + JSON.stringify(data) + ']')
  rules[route] = data
}

exports.read = function () {
  debug('validation:read() rules=[' + JSON.stringify(rules) + ']')
  return rules
}

// Enforce a rule

exports.isValid = function (db, route, field) {
  const fieldKey = Object.keys(field)

  debug('isValid(): route=[' + route + '] key=[' + fieldKey + ']')

  let valid = true
  fieldKey.map(key => {
    debug('isValid(): %s %s should not be empty', key, field[key])
    if (typeof field[key] === 'undefined' || field[key] === '') {
      debug('isValid(): %s should not be empty', key)
      valid = false
    }
  })

  debug('isValid(): %o', valid)
  if (valid === false) {
    return false
  }

  if (undefined === rules[route]) {
    debug('isValid(): No rules for this route, so it is ok for us')
    return true
  }

  if (!required(db, route, fieldKey)) {
    debug('isValid(): Error: missing required field')

    return false
  }

  Object.keys(rules[route]).forEach(f => {
    rules[route][f].forEach(element => {
      if (element === 'required') { return }
      valid &= eval(element)(db, route, fieldKey, field)  // eslint-disable-line
      debug('isValid(): element=[' + element + '] valid=[' + valid + ']')
    })
  })

  debug('isValid(): ' + valid)
  return valid
}
