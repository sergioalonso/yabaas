// Validation API End Point
//

const debug = require('debug')('yabaas:validation')

const express = require('express')
const router = express.Router()

const rules = require('./module')

// Show all rules

router.get('/', function (req, res, next) {
  debug('GET /validation rules:{' + JSON.stringify(rules.read()) + '}')
  res.json({message: '/validation', rules: JSON.stringify(rules.read)})
})

// Create a rule

router.post('/:route', function (req, res, next) {
  debug('POST /validation/' + req.params.route + ' ' + JSON.stringify(req.body))
  rules.create(req.params.route, req.body)
  res.status(201).json({message: 'Created.'})
})

// Delete a rule

router.delete('/', function (req, res, next) {
  debug('DELETE /validation')
  res.json({message: 'Deleted.'})
})

module.exports = router
