// Authentication Module
//

const debug = require('debug')('yabaas:authentication')  // eslint-disable-line

const passport = require('./passport')
const user = require('./user')

exports.register = function (identifier) {
  debug('register() identifier:%o', identifier)

  return new Promise((resolve, reject) => {
    user.create(identifier)
      .then((result) => {
        const token = passport.getToken(identifier)
        debug('register() user._id:%o token:%o', result, token)
        resolve(token)
      })
      .catch((error) => {
        debug('register() ' + error)
        reject(error)
      })
  })
}

exports.signIn = function (identifier) {
  debug('signIn() identifier:%o', identifier)

  return new Promise((resolve, reject) => {
    user.exists(identifier)
      .then((result) => {
        const token = passport.getToken(identifier)
        debug('register() user._id:%o token:%o', result, token)
        resolve(token)
      })
      .catch((error) => {
        debug('signIn() ' + error)
        reject(error)
      })
  })
}
