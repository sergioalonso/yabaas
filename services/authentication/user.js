// User Controller
//

const debug = require('debug')('yabaas:authentication:user')
const persistence = require('../persistence/module')

exports.create = function (identifier) {
  debug('create() identifier:%o', identifier)

  return new Promise((resolve, reject) => {
    persistence.create('register', {email: identifier.email, password: identifier.password})
      .then((user) => {
        debug('create() user:%o', user)
        resolve(user)
      })
      .catch((error) => {
        debug('create() %o', error)
        reject(error)
      })
  })
}

exports.readOne = function (identifier) {
  debug('readOne() identifier:%o', identifier)

  return new Promise((resolve, reject) => {
    persistence.readOne('register', {email: identifier.email, password: identifier.password})
      .then((user) => {
        debug('readOne() user:%o', user)
        resolve(user)
      })
      .catch((error) => {
        debug('readOne() %o', error)
        reject(error)
      })
  })
}

exports.exists = function (identifier) {
  debug('exists() identifier:%o', identifier)

  return new Promise((resolve, reject) => {
    persistence.readOne('register', {email: identifier.email, password: identifier.password})
      .then((user) => {
        debug('exists() user:%o', user)
        if (user === null) reject(new Error('User not found.'))
        resolve(user)
      })
      .catch((error) => {
        debug('exists() %o', error)
        reject(error)
      })
  })
}
