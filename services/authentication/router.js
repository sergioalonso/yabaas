/**
 * Authentication API End Point
 */

const debug = require('debug')('yabaas:authentication')  //eslint-disable-line

const express = require('express')
const router = express.Router()

const authentication = require('./module')

/**
 * @api {post} /authentication/register To register a user
 * @apiName PostRegister
 * @apiGroup Authentication
 *
 * @apiParam {String} email Mandatory user email.
 * @apiParam {String} password Mandatory user password.
 * @apiParamExample {jspn} Request-Example:
 *     POST /authentication/register HTTP/1.1
 *     Content-Type: application/json
 *     Host: localhost:6789
 *
 *     {
 *         email: "user@example.com",
 *         password: "a53c43cdd405e427728a4fef4dc4a8c6746e1b43b5f6b4a7c84791f83e968e959b4e4b74ab91f3094077e3892fd47de1003177d755a98d84f5457bba99aceafc"
 *     }
 *
 * @apiSuccess {json} result Register result.
 * @apiSuccessExample {json} Sucess-Response:
 *     HTTP/1.1 201 Created
 *     Content-Type: application/json; charset=utf-8
 *
 *     {
 *         "message": "Register success.",
 *         "result":
 *         {
 *             "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJpYXQiOjE1MDc1NjY3NDd9.fHDI-T0bRybEU2ztX4OwNwUQ4O1wiIAy3LFnEfvu7nI"
 *         }
 *     }
 *
 * @apiError {json} result Register result.
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 409 Conflict
 *     Content-Type: application/json; charset=utf-8
 *
 *     {
 *         "message": "Register failed.",
 *         "reason": "User already exists."
 *     }
 *
 * @apiExample {HTTPie} HTTPie
 *     http --verbose :6789/authentication/register email=user@example.com password=secret
 *
 */

router.post('/register', (req, res, next) => {
  const email = req.body.email
  const password = req.body.password

  const uid = { email: email, password: password }

  debug('POST /authentication/register uid:%o', uid)

  authentication.register(uid)
    .then((result) => { res.status(201).json({ message: 'Register success.', result: { token: result } }) })
    .catch((reason) => { res.status(409).json({ message: 'Register failed.', reason: reason.toString() }) })
})

/**
 * @api {post} /sign_in To sign in as user
 * @apiName PostSignIn
 * @apiGroup Authentication
 *
 * @apiParam {String} email Mandatory user email.
 * @apiParam {String} password Mandatory user password.
 * @apiParamExample Example params:
 *     {
 *         email: "user@example.com",
 *         password: "a53c43cdd405e427728a4fef4dc4a8c6746e1b43b5f6b4a7c84791f83e968e959b4e4b74ab91f3094077e3892fd47de1003177d755a98d84f5457bba99aceafc"
 *     }
 *
 * @apiSuccess {json} result Register result.
 * @apiSuccessExample {json} Example success:
 *     HTTP/1.1 200 OK
 *
 *     {
 *         "message": "Sign in success.",
 *         "result":
 *         {
 *             "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InVzZXJAZXhhbXBsZS5jb20iLCJpYXQiOjE1MDc1NjY3NDd9.fHDI-T0bRybEU2ztX4OwNwUQ4O1wiIAy3LFnEfvu7nI"
 *         }
 *     }
 *
 * @apiError {json} result Register result.
 * @apiErrorExample {json} Example failed:
 *     HTTP/1.1 409 Conflict
 *
 *     {
 *         "message": "Sign in failed.",
 *         "reason": "Empty email."
 *     }
 *
 * @apiExample {HTTPie} HTTPie
 *     http --verbose :6789/authentication/sign_in email=user@example.com password=secret
 *
 */

router.post('/sign_in', (req, res, next) => {
  const email = req.body.email
  const password = req.body.password

  const uid = { email: email, password: password }

  debug('POST /authentication/sign_in ' + uid)

  authentication.signIn(uid)
    .then((result) => { res.status(200).send({ message: 'Sign in success.', result: { token: result } }) })
    .catch((reason) => { res.status(401).send({ message: 'Sign in failed.', reason: reason.toString() }) })
})

/**
 * dummy end points to test jwt authentication, could be moved to tests?
 */

router.get('/private', (req, res, next) => {
  res.json({message: 'You can see this without a token.'})
})

router.post('/private', (req, res, next) => {
  res.json({message: 'You can see this with a token.'})
})

router.get('/owner/:user', (req, res, next) => {
  res.json({message: 'You can see this if you are the owner.'})
})

module.exports = router
