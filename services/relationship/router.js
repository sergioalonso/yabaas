// Relationship API End Point
//

const debug = require('debug')('yabaas:relationship')  // eslint-disable-line

const express = require('express')
const router = express.Router()

const relationship = require('./module')

router.post('/', function (req, res, next) {
  const data = req.body
  debug('POST /relationship req.body=[%o]', data)

  relationship.create(data)
    .then((result) => {
      debug('201 OK ' + JSON.stringify(result))
      res.status(201).json(result)
    })
    .catch((reason) => {
      debug('409 ERROR ' + JSON.stringify(reason))
      res.status(409).json({message: reason})
    })
})

router.get('/:route/:key/:foreign', function (req, res, next) {
  debug('GET /relationship/' + req.params.route + '/' + req.params.key + '/' + req.params.foreign)

  relationship.read(req.params)
    .then((result) => {
      debug('200 OK ' + JSON.stringify(result))
      res.status(200).json(result)
    })
    .catch((reason) => {
      debug('409 ERROR ' + JSON.stringify(reason))
      res.status(409).json({message: reason})
    })
})

router.delete('/:route/:key/:foreign/:value', function (req, res, next) {
  debug('DELETE /relationship/' + req.params.route + '/' + req.params.key + '/' + req.params.foreign + '/' + req.params.value)

  relationship.delete(req.params)
    .then((result) => {
      debug('201 OK ' + JSON.stringify(result))
      res.status(201).json(result)
    })
    .catch((reason) => {
      debug('409 ERROR ' + JSON.stringify(reason))
      res.status(409).json({message: reason})
    })
})

module.exports = router
