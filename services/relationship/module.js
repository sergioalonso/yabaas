/**
 * Relationship Module
 */

const debug = require('debug')('yabaas:relationship')  // eslint-disable-line

const persistence = require('../persistence/module')

exports.create = data => {
  debug('create() data: %o', data)

  return new Promise((resolve, reject) => {
    if (!persistence.unique('relations', data)) {
      return reject(new Error('Is not unique'))
    }

    persistence.create('relations', {route: data.route, key: data.key, foreign: data.foreign, value: data.value})
      .then(result => resolve({ result: [ result ] }))
      .catch(reason => reject(reason))
  })
}

exports.read = data => {
  debug('read() data: %o', data)

  return new Promise((resolve, reject) => {
    persistence.readAll('relations', {route: data.route, key: data.key, foreign: data.foreign})
      .then(result => {
        let summary = []
        let details = []
        let promises = []
        result.forEach((element) => {
          summary.push(element.value)
          promises.push(
            persistence.readById(data.foreign, element.value)
              .then(result => details.push(result))
              .catch(reason => reject(reason))
          )
        })
        Promise.all(promises).then(() => {
          resolve({ result: summary, details: details })
        })
      })
      .catch(reason => reject(reason))
  })
}

exports.delete = data => {
  debug('delete() data: %o', data)

  return new Promise((resolve, reject) => {
    persistence.delete('relations', data)
      .then(result => resolve(result))
      .catch(reason => reject(reason))
  })
}
