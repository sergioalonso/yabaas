// Email Service Module
//

const debug = require('debug')('yabaas:email')  // eslint-disable-line
const config = require('config')  // eslint-disable-line

const MailDev = require('maildev')
const nodemailer = require('nodemailer')

/**
 * Connect to email server
 */

const maildev = new MailDev({
  smtp: config.get('smtp.port')
})

maildev.listen()

// Handle new emails as they come in
maildev.on('new', email => {
  debug('Received new email with subject: ' + email.subject)
})

// Create a SMTP transporter object
let transporter = nodemailer.createTransport({
  host: config.get('smtp.host'),
  port: config.get('smtp.port'),
  secure: config.get('smtp.secure'),
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
})

transporter.verify(function (e) { debug('verify() ' + e) })

/**
 * Send a mail
 */

exports.send = function (body, callback) {
  debug('send() body=[' + body + ']')

  // Message object
  let message = {
    from: config.get('smtp.from'),
    // Comma separated list of recipients
    to: 'user@example.com',
    // Subject of the message
    subject: 'Hi from yabaas',
    // plaintext body
    text: body
  }

  transporter.sendMail(message, (error, info) => {
    debug('send() message=[' + JSON.stringify(message) + ']')
    if (error) return debug('send() ' + error)
    debug('send() ' + JSON.stringify(info))
    debug('send() Message sent successfully!')
    callback(info)
  })
}

/**
 * Get all received mails
 */

exports.receive = function (callback) {
  debug('receive()')

  maildev.getAllEmail((error, emails) => {
    if (error) return debug('receive() ' + error)
    debug('receive(): There are ' + emails.length + ' emails')
    callback(emails)
  })
}
