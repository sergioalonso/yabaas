// Email Service
//

const debug = require('debug')('yabaas:email')  // eslint-disable-line

const express = require('express')
const router = express.Router()

const email = require('./module')

router.post('/', function (req, res, next) {
  debug('POST /email ')
  res.status(200).json({message: 'post()'})
})

router.get('/', function (req, res, next) {
  debug('GET /email')
  res.status(200).json({message: 'get()'})
})

router.post('/send', function (req, res, next) {
  debug('POST /email/send ' + JSON.stringify(req.body))
  email.send(req.body.message, (response) => {
    debug('POST /email/send message=[' + JSON.stringify(response) + ']')
    res.status(200).json({message: response})
  })
})

router.get('/receive', function (req, res, next) {
  debug('GET /email/receive')
  email.receive((response) => {
    debug('GET /email/receive message=[' + JSON.stringify(response) + ']')
    res.status(200).json({message: response})
  })
})

module.exports = router
