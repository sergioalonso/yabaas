/**
 * Filter Module
 */

const jsonFilters = require(process.cwd() + '/spec/json-filters')

exports.filter = route => jsonFilters.api.filters[route]
